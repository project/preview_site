<?php

namespace Drupal\Tests\preview_site\Functional;

use Drupal\Core\Session\AccountInterface;
use Drupal\filter\Entity\FilterFormat;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\ImageStyleInterface;
use Drupal\node\Entity\Node;
use Drupal\preview_site\Entity\PreviewSiteBuild;
use Drupal\preview_site\Entity\PreviewStrategy;
use Drupal\preview_site\Entity\PreviewStrategyInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\preview_site\Traits\PreviewSiteTestTrait;

/**
 * A class that tests a preview site build and deploy of assets.
 *
 * @group preview_site
 */
class TomeGeneratorAssetsTest extends BrowserTestBase {

  use PreviewSiteTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'preview_site',
    'preview_site_test',
    'tome_static',
    'options',
    'node',
    'image',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The preview site strategy.
   *
   * @var \Drupal\preview_site\Entity\PreviewStrategyInterface
   */
  private PreviewStrategyInterface $previewStrategy;

  /**
   * The user.
   *
   * @var \Drupal\Core\Session\AccountInterface|\Drupal\user\Entity\User|false
   */
  private AccountInterface $user;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $preview_strategy = PreviewStrategy::create([
      'id' => $this->randomMachineName(),
      'label' => $this->randomMachineName(),
      'generate' => 'preview_site_tome',
      'deploy' => 'test',
      'generateSettings' => [],
      'deploySettings' => [
        'prefix' => $this->randomMachineName(),
      ],
    ]);
    $preview_strategy->save();
    $this->previewStrategy = $preview_strategy;
    $filter_format = FilterFormat::create([
      'format' => 'basic_html',
      'name' => 'Basic HTML',
      'weight' => 0,
    ]);
    $filter_format->save();
    $this->createContentType(['type' => 'page']);
    $this->user = $this->drupalCreateUser([
      'administer preview_site strategies',
      'administer preview_site builds',
    ]);
  }

  /**
   * A method to create an image style.
   *
   * @param string $name
   *   The image style name.
   *
   * @return \Drupal\image\ImageStyleInterface
   *   The image style.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function createImageStyle(string $name): ImageStyleInterface {
    $image_style = ImageStyle::create(['name' => $name]);
    $image_style->save();
    return $image_style;
  }

  /**
   * A test to ensure the image style assets use the source path.
   */
  public function testImageStyles(): void {
    $image = $this->getTestFile('image');
    $image_style = $this->createImageStyle('image_style_test');
    $image_url = \Drupal::service('file_url_generator')->transformRelative($image_style->buildUrl($image->getFileUri()));
    $node = Node::create([
      'status' => 1,
      'title' => $this->randomMachineName(),
      'type' => 'page',
      'path' => ['alias' => '/' . $this->randomMachineName()],
      'body' => [
        'format' => 'basic_html',
        'value' => '<img src="' . $image_url . '" />',
      ],
    ]);
    $node->save();
    $build = $this->createPreviewSiteBuild([
      'strategy' => $this->previewStrategy->id(),
      'contents' => [$node],
      'artifacts' => NULL,
      'processed_paths' => NULL,
      'log' => NULL,
    ]);
    $this->drupalLogin($this->user);
    $this->drupalGet($build->toUrl('deploy-form'));
    $this->submitForm([], 'Confirm');
    $this->assertSession()->pageTextContains('The preview site was successfully built');
    $build = PreviewSiteBuild::load($build->id());
    $this->assertNotEmpty($build->get('paths')->filter(fn($processed_path) => str_contains($image_url, $processed_path->value)));
  }

}
