<?php

declare(strict_types=1);

namespace Drupal\Tests\preview_site\Kernel;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Crypt;
use Drupal\node\Entity\Node;
use Drupal\preview_site\Entity\PreviewSiteBuild;

/**
 * Defines a class for testing JSON:API paths.
 *
 * @group preview_site
 */
final class TomeGeneratorJsonApiTest extends TomeGeneratorTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['jsonapi', 'serialization'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig('jsonapi');
  }

  /**
   * Tests generation.
   */
  public function testGeneration() {
    $alias = $this->randomMachineName();
    $node = Node::create([
      'status' => 1,
      'moderation_state' => 'published',
      'title' => $this->randomMachineName(),
      'type' => 'page',
      'path' => ['alias' => '/' . $alias],
      'body' => [
        'format' => 'plain_text',
        'value' => $this->getRandomGenerator()->sentences(10),
      ],
    ]);
    $node->save();

    $build = $this->createPreviewSiteBuild([
      'strategy' => $this->strategy->id(),
      'contents' => [$node],
      'artifacts' => NULL,
      'processed_paths' => NULL,
      'additional_paths' => ['uri' => 'internal:/jsonapi/node/page?fields[]=title'],
      'log' => NULL,
    ]);
    $this->genererateAndDeployBuild($build);
    $build = PreviewSiteBuild::load($build->id());
    $this->assertFalse($build->get('artifacts')->isEmpty());
    $jsonapi_file = sprintf('public://preview-site-test/%s/%s/jsonapi/%s.json', $this->prefix, $build->uuid(), Crypt::hashBase64('/jsonapi/node/page?' . urlencode('fields[0]') . '=title'));
    $this->assertTrue(file_exists($jsonapi_file));

    $data = Json::decode((file_get_contents($jsonapi_file)));
    $this->assertEquals($node->label(), $data['data'][0]['attributes']['title']);
  }

}
