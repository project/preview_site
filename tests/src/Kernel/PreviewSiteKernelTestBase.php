<?php

namespace Drupal\Tests\preview_site\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\preview_site\Traits\PreviewSiteTestTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Defines a base class for preview site tests.
 *
 * @group preview_site
 */
abstract class PreviewSiteKernelTestBase extends KernelTestBase {

  use UserCreationTrait;
  use PreviewSiteTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'preview_site',
    'preview_site_test',
    'text',
    'datetime',
    'file',
    'field',
    'filter',
    'path_alias',
    'options',
    'tome_static',
    'entity_usage',
    'dynamic_entity_reference',
    'link',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();
    $this->installSchema('system', ['sequences']);
    $this->installEntitySchema('user');
    $this->installEntitySchema('file');
    $this->installEntitySchema('path_alias');
    $this->installEntitySchema('preview_site_build');
    $this->installSchema('file', ['file_usage']);
    $this->installSchema('entity_usage', ['entity_usage']);
  }

}
