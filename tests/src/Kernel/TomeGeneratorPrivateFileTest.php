<?php

namespace Drupal\Tests\preview_site\Kernel;

use Drupal\file\Entity\File;
use Drupal\filter\Entity\FilterFormat;
use Drupal\node\Entity\Node;
use Drupal\preview_site\Entity\PreviewSiteBuild;
use Drupal\user\Entity\User;

/**
 * Defines a class for testing tome generator with private files.
 *
 * @group preview_site
 */
class TomeGeneratorPrivateFileTest extends TomeGeneratorTestBase {

  public function setUp(): void {
    parent::setUp();
    $filter_format = FilterFormat::create([
      'format' => 'basic_html',
      'name' => 'Basic HTML',
      'weight' => 0,
    ]);
    $filter_format->save();
  }

  /**
   * Tests generation, with link to private file.
   */
  public function testGenerationWithPrivateFile(): void {
    $uri = 'private://test-file.txt';

    /** @var \Drupal\Core\File\FileSystemInterface $fileSystem */
    $fileSystem = \Drupal::service('file_system');
    $fileSystem->copy($this->getTestFile('text')->getFileUri(), $uri);

    $file = File::create([
      'uri' => $uri,
      'filename' => basename($uri),
      'uid' => User::getAnonymousUser()->id(),
    ]);
    $file->save();

    \Drupal::service('session')->set('anonymous_allowed_file_ids', [
      $file->id() => $file->id(),
    ]);

    $url = $file->createFileUrl();
    $node = Node::create([
      'status' => 0,
      'moderation_state' => 'draft',
      'title' => $this->randomMachineName(),
      'type' => 'page',
      'body' => [
        'format' => 'basic_html',
        'value' => '<a href="' . $url . '">File</a>',
      ],
    ]);
    $node->save();

    $build = $this->createPreviewSiteBuild([
      'strategy' => $this->strategy->id(),
      'contents' => [$node],
      'artifacts' => NULL,
      'processed_paths' => NULL,
      'log' => NULL,
    ]);

    $this->genererateAndDeployBuild($build);

    $build = PreviewSiteBuild::load($build->id());
    $node_static_file = $this->getGeneratedFileForEntity($node, $build);
    $this->assertTrue(file_exists($node_static_file));
    $this->assertTrue(file_exists(sprintf('public://preview-site-test/%s/%s%s', $this->prefix, $build->uuid(), $url)));
  }

}
