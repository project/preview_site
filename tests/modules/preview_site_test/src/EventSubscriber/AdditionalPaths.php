<?php

declare(strict_types=1);

namespace Drupal\preview_site_test\EventSubscriber;

use Drupal\Core\State\StateInterface;
use Drupal\preview_site\EventSubscribers\AdditionalPathsEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Defines an event listener for preview site additional paths.
 */
final class AdditionalPaths implements EventSubscriberInterface {

  const BUILD_UUID = '60dbb5e4-4947-4743-bfc1-3c1ad273eb39';

  /**
   * Constructs a new AdditionalPaths.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   State.
   */
  public function __construct(protected StateInterface $state) {
  }

  /**
   * Event listener.
   *
   * @param \Drupal\preview_site\EventSubscribers\AdditionalPathsEvent $event
   *   Passed event.
   */
  public function onAdditionalPaths(AdditionalPathsEvent $event): void {
    $build = $event->getBuild();
    if ($build->uuid() === self::BUILD_UUID) {
      $event->addAdditionalPath('/core/misc/druplicon.png');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [AdditionalPathsEvent::class => ['onAdditionalPaths']];
  }

}
