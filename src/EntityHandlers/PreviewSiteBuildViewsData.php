<?php

declare(strict_types = 1);

namespace Drupal\preview_site\EntityHandlers;

use Drupal\preview_site\Plugin\views\field\ItemCount;
use Drupal\preview_site\Plugin\views\field\ItemLinks;
use Drupal\views\EntityViewsData;

/**
 * Provides views data for the preview_site_build entity type.
 */
class PreviewSiteBuildViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['preview_site_build'][ItemCount::PLUGIN_ID]['field'] = [
      'id' => ItemCount::PLUGIN_ID,
      'title' => $this->t('Item count'),
      'help' => $this->t('The number of entities in the preview site build'),
      'additional fields' => [
        'bid' => [
          'table' => 'preview_site_build',
          'field' => 'bid',
        ],
      ],
    ];
    $data['preview_site_build'][ItemLinks::PLUGIN_ID]['field'] = [
      'id' => ItemLinks::PLUGIN_ID,
      'title' => $this->t('Item links'),
      'help' => $this->t('A dropbutton of links to items in the preview site build'),
      'additional fields' => [
        'bid' => [
          'table' => 'preview_site_build',
          'field' => 'bid',
        ],
      ],
    ];

    return $data;
  }

}
