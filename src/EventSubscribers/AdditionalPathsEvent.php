<?php

declare(strict_types=1);

namespace Drupal\preview_site\EventSubscribers;

use Drupal\Component\EventDispatcher\Event;
use Drupal\preview_site\Entity\PreviewSiteBuildInterface;

/**
 * Defines a class for an additional paths event.
 */
final class AdditionalPathsEvent extends Event {

  /**
   * Constructs a new AdditionalPathsEvent.
   */
  public function __construct(
    protected PreviewSiteBuildInterface $build,
    protected array $paths = [],
  ) { }

  /**
   * Gets additional paths.
   *
   * @return array
   *   Additional paths.
   */
  public function getAdditionalPaths(): array {
    return $this->paths;
  }

  /**
   * Adds an additional path.
   *
   * @param string $path
   *   Path to add.
   *
   * @return $this
   *   The current instance for chaining.
   */
  public function addAdditionalPath(string $path): self {
    $this->paths[] = $path;
    return $this;
  }

  /**
   * Gets the build.
   *
   * @return \Drupal\preview_site\Entity\PreviewSiteBuildInterface
   *   The build.
   */
  public function getBuild(): PreviewSiteBuildInterface {
    return $this->build;
  }

}
