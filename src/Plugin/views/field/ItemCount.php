<?php

declare(strict_types = 1);

namespace Drupal\preview_site\Plugin\views\field;

use Drupal\preview_site\Entity\PreviewSiteBuildInterface;
use Drupal\views\Plugin\views\field\NumericField;
use Drupal\views\ResultRow;

/**
 * Computes the number of items on the preview site build.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField(\Drupal\preview_site\Plugin\views\field\ItemCount::PLUGIN_ID)
 * @property \Drupal\views\Plugin\views\query\Sql $query
 */
final class ItemCount extends NumericField {

  public const PLUGIN_ID = 'preview_site_item_count';

  /**
   * {@inheritdoc}
   */
  public function getValue(ResultRow $values, $field = NULL): int {
    $entity = $values->_entity;
    if (!$entity instanceof PreviewSiteBuildInterface) {
      return 0;
    }

    return $entity->getItemCount();
  }

  /**
   * {@inheritdoc}
   */
  public function query(): void {
    // Don't call parent and add additional fields only since this is a virtual
    // field.
    $this->addAdditionalFields();
  }

}
