<?php

declare(strict_types = 1);

namespace Drupal\preview_site\Plugin\views\field;

use Drupal\preview_site\Entity\PreviewSiteBuildInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Displays a dropbutton of links to items in the build.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField(\Drupal\preview_site\Plugin\views\field\ItemLinks::PLUGIN_ID)
 * @property \Drupal\views\Plugin\views\query\Sql $query
 */
final class ItemLinks extends FieldPluginBase {

  public const PLUGIN_ID = 'preview_site_item_links';


  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity = $this->getEntity($values);
    if (!$entity instanceof PreviewSiteBuildInterface) {
      return '';
    }

    $deployment_uri = $entity->getDeploymentBaseUri();
    if (!$deployment_uri) {
      return ['#markup' => ''];
    }
    return [
      '#type' => 'operations',
      '#links' => $entity->getItemLinks(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query(): void {
    // Don't call parent and add additional fields only since this is a virtual
    // field.
    $this->addAdditionalFields();
  }

}
