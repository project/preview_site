<?php

namespace Drupal\preview_site\Plugin\PreviewSite\Generate;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\preview_site\Entity\PreviewSiteBuildInterface;
use Drupal\preview_site\EntityHandlers\ParentNegotiation\ParentNegotiationInterface;
use Drupal\preview_site\Generate\CouldNotWriteFileException;
use Drupal\preview_site\Generate\FileCollection;
use Drupal\preview_site\Generate\FileHelper;
use Drupal\preview_site\Generate\GeneratePluginBase;
use Drupal\tome_base\PathTrait;
use Drupal\tome_static\StaticGeneratorInterface;
use Drupal\tome_static\TomeStaticUrlHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a default generator plugin.
 *
 * @PreviewSiteGenerate(
 *   id = "preview_site_tome",
 *   title = @Translation("Tome"),
 *   description = @Translation("Use Tome static for generation."),
 * )
 */
class TomeGenerator extends GeneratePluginBase {

  use PathTrait;

  /**
   * The static generator.
   *
   * @var \Drupal\preview_site\Generate\TomeStaticExtension
   */
  protected $static;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The request preparer.
   *
   * @var \Drupal\tome_static\RequestPreparer
   */
  protected $requestPreparer;

  /**
   * File system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * State.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * File saved listener.
   *
   * @var \Drupal\preview_site\EventSubscribers\TomeStaticListener
   */
  protected $fileSavedListener;

  /**
   * Static cache.
   *
   * @var \Drupal\tome_static\StaticCacheInterface|\Drupal\Core\Cache\CacheBackendInterface
   */
  protected $tomeStaticCache;

  /**
   * Entity usage.
   *
   * @var \Drupal\entity_usage\EntityUsageInterface
   */
  protected $entityUsage;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The mime type guesser.
   *
   * @var \Drupal\Core\File\MimeType\MimeTypeGuesser
   */
  protected $mimeTypeGuesser;

  /**
   * The tracked entity Ids.
   *
   * @var array
   */
  protected $trackedEntityIds = [];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->static = $container->get('preview_site.tome_static');
    $instance->requestPreparer = $container->get('tome_static.request_preparer');
    $instance->requestStack = $container->get('request_stack');
    $instance->fileSystem = $container->get('file_system');
    $instance->state = $container->get('state');
    $instance->fileSavedListener = $container->get('preview_site.tome_file_saved_listener');
    $instance->tomeStaticCache = $container->get('cache.tome_static');
    $instance->entityUsage = $container->get('entity_usage.usage');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->mimeTypeGuesser = $container->get('file.mime_type.guesser');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function generateBuildForItem(PreviewSiteBuildInterface $build, EntityReferenceItem $item, string $base_url, QueueInterface $asset_queue): FileCollection {
    // We don't load the content entity yet, so that loading occurs in the scope
    // of the tome-generation.
    $path = sprintf('_entity:%s:%s:%s', $item->target_type, $build->language()->getId(), $item->target_id);
    return $this->generateBuildForPath($build, $path, $base_url, $asset_queue);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareBuild(PreviewSiteBuildInterface $build, ?string $base_url) {
    $this->state->set(StaticGeneratorInterface::STATE_KEY_BUILDING, TRUE);
    $this->state->set(StaticGeneratorInterface::STATE_KEY_URL, $base_url);
    $this->static->setStaticDirectory('private://preview-site/' . $build->uuid());
    $this->static->prepareStaticDirectory();
    $this->tomeStaticCache->deleteAll();
  }

  /**
   * {@inheritdoc}
   */
  public function completeBuild(PreviewSiteBuildInterface $build) {
    $this->state->set(StaticGeneratorInterface::STATE_KEY_BUILDING, FALSE);
    $this->static->setStaticDirectory('private://preview-site/' . $build->uuid());
    $this->static->cleanupStaticDirectory();
  }

  /**
   * {@inheritdoc}
   */
  public function entityPreload(PreviewSiteBuildInterface $build, array $ids, string $entity_type_id, EntityTypeManagerInterface $entity_type_manager): array {
    if ($entity_type_id === 'paragraph') {
      // ERR ensures the right revision is loaded here.
      return [];
    }
    if ($this->state->get(StaticGeneratorInterface::STATE_KEY_BUILDING) && $this->static->isGenerating()) {
      return parent::entityPreload($build, $ids, $entity_type_id, $entity_type_manager);
    }
    // We don't want to do anything unless there's a tome build in progress.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function generateBuildForPath(PreviewSiteBuildInterface $build, string $path, string $base_url, QueueInterface $asset_queue): FileCollection {
    $request = $this->requestStack->getCurrentRequest();
    $build->markPathAsProcessed($path);
    $this->static->setStaticDirectory('private://preview-site/' . $build->uuid());
    $original_params = TomeStaticUrlHelper::setBaseUrl($request, $base_url);
    $simulatedHostAndScheme = $request->getSchemeAndHttpHost();

    $this->requestPreparer->prepareForRequest();
    $this->fileSavedListener->resetLastFileWritten();
    try {
      $this->static->setIsGenerating(TRUE);
      $invoke_paths = $this->static->requestPath($path);
    }
    catch (\Exception $e) {
      $build->addLogEntry(sprintf('ERROR: Exception caught when requesting path %s in %s, line %s: %s',
        $path,
        $e->getFile(),
        $e->getLine(),
        $e->getMessage()
      ), FALSE);
      $build->deploymentFailed($this->state, FALSE);
      TomeStaticUrlHelper::restoreBaseUrl($request, $original_params);
      return new FileCollection();
    }
    finally {
      $this->static->setIsGenerating(FALSE);
    }

    TomeStaticUrlHelper::restoreBaseUrl($request, $original_params);

    if (!($destination = $this->fileSavedListener->getLastFileWritten())) {
      $build->addLogEntry(sprintf('WARNING: The anonymous user does not have access to view the latest revision for %s: skipped.',
        $path
      ));
      return new FileCollection();
    }

    // If the path being processed is an image (likely image style), then
    // no further processing needs to happen. The path can be returned.
    // For image styles they'll be generated via the request above.
    $sanitised_path = $this->static->sanitizePath(urldecode($path));
    if ($this->isPathAnImage($sanitised_path)) {
      $collection = new FileCollection();
      $build->markPathAsProcessed($sanitised_path);
      $collection->addPath($sanitised_path);
      return $collection;
    }

    $this->static->resetCopiedPaths();
    $remaining_assets = $this->static->exportPaths($invoke_paths);
    $copied_paths = $this->static->getCopiedPaths();

    foreach ($remaining_assets as $asset) {
      $asset_destination = $this->static->getDestination($asset);
      if (substr($asset_destination, -1 * strlen('/index.html')) === '/index.html' && !preg_match('@^/media/oembed\?@', $asset)) {
        // We don't want to crawl the site for other content, we only want to
        // get CSS, Javascript, images etc, unless it is a redirect.
        if (!$this->static->getCache()->isRedirect($simulatedHostAndScheme, $asset)) {
          continue;
        }
      }
      if (!$build->hasPathBeenProcessed($asset)) {
        $asset_queue->createItem($asset);
      }
    }

    try {
      $collection = new FileCollection(FileHelper::createFromExistingFile($destination));
      foreach (array_unique($copied_paths) as $copied_path) {
        // Remove any relative slashes (./) but retain ../.
        $copied_path = preg_replace('@(?<!\.)\./@', '', $copied_path);
        if ($build->hasPathBeenProcessed($copied_path)) {
          continue;
        }
        $build->markPathAsProcessed($copied_path);
        $collection->addPath($copied_path);
      }
      return $collection;
    }
    catch (CouldNotWriteFileException $e) {
      $build->addLogEntry(sprintf('ERROR: Exception caught when attempting to create file from %s: %s',
        $path,
        $e->getMessage()
      ), FALSE);
      $build->deploymentFailed($this->state, FALSE);
    }
    return new FileCollection();
  }

  /**
   * A method to determine whether a path is an image.
   *
   * @param string $path
   *   The path.
   *
   * @return bool
   *   The result.
   */
  protected function isPathAnImage(string $path): bool {
    $mime_type = $this->mimeTypeGuesser->guessMimeType($path);
    if (!empty($mime_type) && str_contains($mime_type, '/')) {
      [$prefix] = explode('/', $mime_type);
      return $prefix === 'image';
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function entityAccess(PreviewSiteBuildInterface $build, ContentEntityInterface $entity, AccountInterface $account, EntityTypeManagerInterface $entityTypeManager): AccessResultInterface {
    return AccessResult::allowedIf($this->static->isGenerating() && $this->entityIsRelevantToBuild($build, $entity))->setCacheMaxAge(0);
  }

  /**
   * Checks if an entity is relevant to this preview-site build.
   *
   * @param \Drupal\preview_site\Entity\PreviewSiteBuildInterface $build
   *   Build.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity.
   *
   * @return bool
   *   TRUE if is available.
   */
  protected function entityIsRelevantToBuild(PreviewSiteBuildInterface $build, ContentEntityInterface $entity): bool {
    return !empty($build->getMatchingContents([$entity->id()], $entity->getEntityTypeId())) || ($this->resetTrackedEntityIds() && $this->hasRelatedUsage($build, $entity));
  }

  /**
   * Checks if an entity has a related usage to content in the build.
   *
   * @param \Drupal\preview_site\Entity\PreviewSiteBuildInterface $build
   *   Build.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity.
   *
   * @return bool
   *   TRUE if is available.
   */
  protected function hasRelatedUsage(PreviewSiteBuildInterface $build, ContentEntityInterface $entity) : bool {
    static $depth = 1;
    // If this function has been called 10 times or greater recursively,
    // we don't track any further entity usage.
    if ($depth >= 10) {
      $this->setTrackedEntity($entity, FALSE);
      return FALSE;
    }
    if ($this->hasTrackedEntity($entity)) {
      return $this->getTrackedEntity($entity);
    }
    if ($negotiator = $this->entityTypeManager->getHandler($entity->getEntityTypeId(), self::PARENT_NEGOTIATION_HANDLER)) {
      assert($negotiator instanceof ParentNegotiationInterface);
      if (($parent = $negotiator->getParentEntity($entity)) &&
          !empty($build->getMatchingContents([$parent->id()], $parent->getEntityTypeId()))) {
        $this->setTrackedEntity($entity);
        $this->setTrackedEntity($parent);
        return TRUE;
      }
    }
    $usages = $this->entityUsage->listSources($entity);
    foreach ($usages as $entity_type_id => $items) {
      $entity_ids = array_keys($items);
      if (!empty($build->getMatchingContents($entity_ids, $entity_type_id))) {
        $this->setTrackedEntity($entity);
        return TRUE;
      }
      $dependant_entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
      if (!$dependant_entity_type->entityClassImplements(ContentEntityInterface::class)) {
        continue;
      }
      foreach ($this->entityTypeManager->getStorage($entity_type_id)->loadMultiple($entity_ids) as $dependant_entity) {
        // Increment the depth as we call this function.
        $depth++;
        if ($this->hasRelatedUsage($build, $dependant_entity)) {
          $this->setTrackedEntity($entity);
          return TRUE;
        }
      }
    }
    $this->setTrackedEntity($entity, FALSE);
    return FALSE;
  }

  /**
   * A method to generate a tracked entity id.
   *
   * The id is just a combination of the type and id.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The tracked entity.
   *
   * @return string
   *   The tracked entity id.
   */
  protected function generateTrackedEntityId(EntityInterface $entity): string {
    return "{$entity->getEntityTypeId()}:{$entity->id()}";
  }

  /**
   * A method to check whether the entity has been tracked.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The tracked entity.
   *
   * @return bool
   *   Has the entity been tracked.
   */
  protected function hasTrackedEntity(EntityInterface $entity): bool {
    return array_key_exists($this->generateTrackedEntityId($entity), $this->trackedEntityIds);
  }

  /**
   * A method to get the tracked entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The tracked entity.
   *
   * @return bool
   *   The tracked entity value - applies.
   */
  protected function getTrackedEntity(EntityInterface $entity): bool {
    return $this->hasTrackedEntity($entity) ? $this->trackedEntityIds[$this->generateTrackedEntityId($entity)] : FALSE;
  }

  /**
   * A method to set the tracked entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to be tracked.
   * @param bool $applies
   *   Whether the entity that is being tracked applies.
   */
  protected function setTrackedEntity(EntityInterface $entity, bool $applies = TRUE): void {
    $this->trackedEntityIds[$this->generateTrackedEntityId($entity)] = $applies;
  }

  /**
   * A method to reset the tracked entity ids.
   *
   * @return bool
   *   The result.
   */
  protected function resetTrackedEntityIds(): bool {
    $this->trackedEntityIds = [];
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function entityQueryAlter(PreviewSiteBuildInterface $build, AlterableInterface $query, EntityTypeInterface $entity_type) {
    if (!$this->static->isGenerating()) {
      return;
    }
    parent::entityQueryAlter($build, $query, $entity_type);
    if (!($published_key = $entity_type->getKey('published'))) {
      return;
    }
    if (!($id_key = $entity_type->getKey('id'))) {
      return;
    }
    $status_field = $entity_type->getDataTable() . '.' . $published_key;
    $conditions = &$query->conditions();
    foreach ($conditions as $key => $condition) {
      if ($key === '#conjunction') {
        continue;
      }
      if ($condition['field'] !== $status_field) {
        continue;
      }
      $ids_of_type = $build->getContentsOfType($entity_type->id());
      if (!$ids_of_type) {
        continue;
      }
      $group = $query->orConditionGroup()
        ->condition($condition['field'], $condition['value'], $condition['operator'])
        ->condition($entity_type->getDataTable() . '.' . $id_key, $ids_of_type, 'IN');
      $conditions[$key] = [
        'field' => $group,
        'value' => NULL,
        'operator' => '=',
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getArtifactBasePath(PreviewSiteBuildInterface $build): string {
    return 'private://preview-site/' . $build->uuid();
  }

  /**
   * {@inheritdoc}
   */
  public function alterUrlToDeployedItem(string $url, PreviewSiteBuildInterface $build): string {
    return $url . '/index.html';
  }

}
