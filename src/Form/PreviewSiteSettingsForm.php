<?php

declare(strict_types=1);

namespace Drupal\preview_site\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * A form for managing the preview site settings.
 *
 * @codeCoverageIgnore
 */
class PreviewSiteSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['preview_site.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'preview_site_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['proactive_stale_check'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Proactive stale check'),
      '#description' => $this->t("By enabling this option a check will be run on every entity save to find 'built' preview sites that are connected and mark them as stale."),
      '#default_value' => $this->config('preview_site.settings')->get('proactive_stale_check'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('preview_site.settings')->set('proactive_stale_check', $form_state->getValue('proactive_stale_check'))->save();
    parent::submitForm($form, $form_state);
  }

}
