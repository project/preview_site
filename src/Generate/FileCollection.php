<?php

namespace Drupal\preview_site\Generate;

use Drupal\file\FileInterface;

/**
 * Defines a class for a collection of files.
 */
class FileCollection implements \IteratorAggregate {

  /**
   * @var \Drupal\file\FileInterface[]
   */
  private $files = [];

  /**
   * Static paths to use as artifacts.
   *
   * @var string[]
   */
  private $paths = [];

  /**
   * Constructs a new FileCollection.
   *
   * @param \Drupal\file\FileInterface $files
   *   The files in the collection.
   */
  public function __construct(FileInterface ...$files) {
    $this->files = $files;
  }

  /**
   * {@inheritdoc}
   */
  public function getIterator(): \Traversable {
    return new \ArrayIterator($this->files);
  }

  /**
   * Adds a file.
   *
   * @param \Drupal\file\FileInterface $file
   *   File to add.
   *
   * @return $this
   */
  public function addFile(FileInterface $file) : FileCollection {
    $this->files[] = $file;
    return $this;
  }

  /**
   * Adds a path to the path artifacts.
   *
   * @param string $path
   *   Path artifact.
   *
   * @return $this
   */
  public function addPath(string $path): FileCollection {
    $this->paths[] = $path;
    $this->paths = array_unique($this->paths);
    return $this;
  }

  /**
   * Gets paths to be added as artifacts without copying.
   *
   * @return string[]
   *   Value of Paths.
   */
  public function getPaths(): array {
    return $this->paths;
  }

}
