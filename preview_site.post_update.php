<?php

/**
 * @file
<<<<<<< ours
 * Post update hooks for preview_site.
 */

declare(strict_types=1);

/**
 * Clear caches to use the new views data handler.
 */
function preview_site_post_update_clear_entity_cache(): void {
  // An empty update will flush caches.
}

/**
 * Clear any pending deployment queues due to the data format change.
 */
function preview_site_post_update_clear_deployment_queue(): void {
  foreach (\Drupal::entityTypeManager()->getStorage('preview_site_build')->getQuery()->accessCheck(FALSE)->execute() as $id) {
    $queue = \Drupal::queue('preview_site_deploy:', $id);
    $queue->deleteQueue();
  }
}
